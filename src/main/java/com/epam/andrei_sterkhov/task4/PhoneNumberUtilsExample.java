package com.epam.andrei_sterkhov.task4;

import java.util.List;
import java.util.regex.Pattern;

public class PhoneNumberUtilsExample {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("[+][\\d]\\(\\d{3}\\)[ ]\\d{3}(?:[ ]\\d{2}){2}");
        PhoneNumberUtils finder = new PhoneNumberUtils();

        List<String> list = finder.getPhoneNumbersFromText("src/main/java/com/epam/andrei_sterkhov/task4/sample.txt", pattern);

        list = finder.getPhoneNumbersOnlyWithDigits(list);

        finder.writePhoneNumbersToFile("src/main/java/com/epam/andrei_sterkhov/task4/phoneNumbers.txt", "\n", list);
    }
}

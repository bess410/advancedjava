package com.epam.andrei_sterkhov.task4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class PhoneNumberUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(PhoneNumberUtils.class);
    public List<String> getPhoneNumbersFromText(String from, Pattern pattern){
        List<String> phoneNumbers = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(from))) {
            String string;
            Matcher matcher;
            while (reader.ready()) {
                string = reader.readLine();
                matcher = pattern.matcher(string);
                while (matcher.find()) {
                    phoneNumbers.add(matcher.group());
                }
            }
        } catch (IOException e) {
            LOGGER.error("{}", e);
        }

        return phoneNumbers;
    }

    public boolean writePhoneNumbersToFile(String to, String delimiter, List<String> phoneNumbers){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(to))) {
            for (String str : phoneNumbers) {
                writer.write(str);
                writer.write(delimiter);
            }
            return true;
        } catch (IOException e) {
            LOGGER.error("{}", e);
            return false;
        }
    }

    public List<String> getPhoneNumbersOnlyWithDigits(List<String> phoneNumbers) {
        return phoneNumbers.stream().map(this::deleteNotDigits).collect(Collectors.toList());
    }

    private String deleteNotDigits(String string) {
        return string.replaceAll("[^\\d]", "");
    }
}

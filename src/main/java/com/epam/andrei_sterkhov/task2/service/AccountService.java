package com.epam.andrei_sterkhov.task2.service;

import com.epam.andrei_sterkhov.task2.dao.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Path;


public class AccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);
    public Account createAccount(String userName, Long balance) {
        return new Account(userName, balance);
    }

    public boolean writeAccount(Account account, String folderName) {
        FileOutputStream fout;
        try {
            fout = new FileOutputStream(folderName + "/" + account.getId() + ".acc");
        } catch (FileNotFoundException e) {
            LOGGER.error("File not found", e);
            return false;
        }
        try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
            oos.writeObject(account);
        } catch (IOException e) {
            LOGGER.error("Writing object is felt", e);
            return false;
        }
        return true;
    }

    public Account readFromFile(Path filename) {
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(String.valueOf(filename));
        } catch (FileNotFoundException e) {
            LOGGER.error("File not found", e);
        }
        try (ObjectInputStream ois = new ObjectInputStream(fin)) {
            return (Account) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            LOGGER.error("Class not found or error of file reading", e);
        }
        return null;
    }
}

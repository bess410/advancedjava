package com.epam.andrei_sterkhov.task2.dao;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account implements Serializable {
    private static final AtomicLong NEXT_ID = new AtomicLong(0);
    private final long id = NEXT_ID.incrementAndGet();
    private String userName;
    private Long balance;
    private transient Lock lock = new ReentrantLock();

    public Account(String userName, Long balance) {
        this.userName = userName;
        this.balance = balance;
    }

    public Account() {

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Lock getLock() {
        return lock;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", balance=" + balance +
                "}\n";
    }
}

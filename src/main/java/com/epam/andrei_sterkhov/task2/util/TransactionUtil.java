package com.epam.andrei_sterkhov.task2.util;

import com.epam.andrei_sterkhov.task2.dao.Account;
import com.epam.andrei_sterkhov.task2.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;

public class TransactionUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionUtil.class);
    private final int transfersNumber;
    private TransferValidator validator;
    private List<Account> accounts;
    private ExecutorService executor;
    private static final AtomicLong COUNTER = new AtomicLong(0);

    public TransactionUtil(List<Account> accounts, int transfersNumber, ExecutorService executor) {
        this.accounts = accounts;
        this.transfersNumber = transfersNumber;
        this.executor = executor;
        validator = new TransferValidator(accounts);
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public AtomicLong getCounter() {
        return COUNTER;
    }

    public void transfer(Account from, Account to, Long amount) {
        try {
            validator.isTryTransferToSameAccount(from, to);
        } catch (TryTransferToSameAccountException e) {
            return;
        }
        // Получаем наши локи
        Lock lock1 = from.getId() > to.getId() ? to.getLock() : from.getLock();
        Lock lock2 = from.getId() > to.getId() ? from.getLock() : to.getLock();

        // Пытаемся залочить
        if (lock1.tryLock()) {
            try {
                lock2.lock();
                    try {
                        LOGGER.info("\n\tFrom: {}\tto: {}\twith amount {} at the start of transfer"
                                , from, to, amount);
                        // Проверяем
                        validator.validate(from, to, amount);

                        COUNTER.incrementAndGet();
                        // Без блокировки COUNTER увеличиваем счетчик
                        if (COUNTER.get() <= transfersNumber) {
                            LOGGER.info("Transfer number is: {}", COUNTER);
                        } else {
                            // Убиваем все потоки
                            executor.shutdownNow();
                            LOGGER.info("All transfers were interrupted.");
                            return;
                        }
                        from.setBalance(from.getBalance() - amount);
                        to.setBalance(to.getBalance() + amount);
                        LOGGER.info("\n\tFrom: {}\tto: {}\twith amount {} was successful"
                                , from, to, amount);
                    } catch (AccountIsNullException | TryTransferToSameAccountException |
                            AmountIsGreatThanBalanceOnAccountException | WrongAccountException |
                            AmountIsNegativeOrZeroException e) {
                        LOGGER.error("Validate error: {}\n" +
                                        "\tAccount from: {}" +
                                        "\tAccount to: {}" +
                                        "\tAmount is {}"
                                , e.getMessage(), from, to, amount);
                    } finally {
                        lock2.unlock();
                    }
            } finally {
                lock1.unlock();
            }
        }
    }
}

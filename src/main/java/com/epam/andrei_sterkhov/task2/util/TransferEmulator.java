package com.epam.andrei_sterkhov.task2.util;

import com.epam.andrei_sterkhov.task2.generator.Generator;

public class TransferEmulator implements Runnable {
    private TransactionUtil transactionUtil;
    private Generator generator = new Generator();

    public TransferEmulator(TransactionUtil transactionUtil) {
        this.transactionUtil = transactionUtil;
        generator.setAccounts(transactionUtil.getAccounts());
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            transactionUtil.transfer(generator.getRandomAccount(),
                    generator.getRandomAccount(), generator.getRandomAmount());
        }
    }
}

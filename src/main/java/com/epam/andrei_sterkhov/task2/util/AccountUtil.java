package com.epam.andrei_sterkhov.task2.util;

import com.epam.andrei_sterkhov.task2.dao.Account;
import com.epam.andrei_sterkhov.task2.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AccountUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountUtil.class);
    private String folderName;
    private AccountService service = new AccountService();

    public AccountUtil(String folderName) {
        this.folderName = folderName;
    }

    public Account createAccount(String name, Long balance) {
        return service.createAccount(name, balance);
    }

    public Long getBalanceFromFolder() {
        return readAccountsFromFolder().stream()
                .map(Account::getBalance)
                .mapToLong(Long::valueOf)
                .sum();
    }

    public Long getBalanceFromList(List<Account> accounts) {
        return accounts.stream()
                .map(Account::getBalance)
                .mapToLong(Long::valueOf)
                .sum();
    }

    public List<Account> readAccountsFromFolder() {
        List<Account> accounts = null;
        try (Stream<Path> paths = Files.walk(Paths.get(folderName))) {
            accounts = paths
                    .filter(Files::isRegularFile)
                    .map(service::readFromFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            LOGGER.error("Error to file reading", e);
        }
        return accounts;
    }

    public boolean writeAccountsToFolder(List<Account> accounts) {
        for (Account account : accounts) {
            if (!service.writeAccount(account, folderName)) {
                LOGGER.info("Error accounts writing");
                return false;
            }
        }
        LOGGER.info("Accounts wrote to folder successfully");
        return true;
    }
}

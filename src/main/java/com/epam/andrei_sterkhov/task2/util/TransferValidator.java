package com.epam.andrei_sterkhov.task2.util;

import com.epam.andrei_sterkhov.task2.dao.Account;
import com.epam.andrei_sterkhov.task2.exception.*;

import java.util.List;
import java.util.Objects;

public class TransferValidator {
    private List<Account> accounts;

    public TransferValidator(List<Account> accounts) {
        this.accounts = accounts;
    }

    public void validate(Account from, Account to, Long amount)
            throws AccountIsNullException,
            WrongAccountException,
            TryTransferToSameAccountException,
            AmountIsGreatThanBalanceOnAccountException, AmountIsNegativeOrZeroException {

        isAccountNull(from);
        isAccountNull(to);
        isWrongAccount(from);
        isWrongAccount(to);
        isTryTransferToSameAccount(from, to);
        isAmountGreatThanBalance(from, amount);
        isAmountNegativeOrZero(amount);
    }

    private void isAccountNull(Account account) throws AccountIsNullException {
        if (Objects.isNull(account)) {
            throw new AccountIsNullException("Account is null");
        }
    }

    private void isWrongAccount(Account account) throws WrongAccountException {
        if (!accounts.contains(account)) {
            throw new WrongAccountException("Account is wrong");
        }
    }

    public void isTryTransferToSameAccount(Account from, Account to) throws TryTransferToSameAccountException {
        if (from == to) {
            throw new TryTransferToSameAccountException("Try to transfer to same account");
        }
    }

    private void isAmountGreatThanBalance(Account from, Long amount) throws AmountIsGreatThanBalanceOnAccountException {
        if (from.getBalance() < amount) {
            throw new AmountIsGreatThanBalanceOnAccountException("Amount is great than account's balance");
        }
    }

    private void isAmountNegativeOrZero(Long amount) throws AmountIsNegativeOrZeroException {
        if (amount <= 0) {
            throw new AmountIsNegativeOrZeroException("Amount is negative or zero");
        }
    }
}

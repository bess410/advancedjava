package com.epam.andrei_sterkhov.task2;

import com.epam.andrei_sterkhov.task2.dao.Account;
import com.epam.andrei_sterkhov.task2.generator.Generator;
import com.epam.andrei_sterkhov.task2.util.AccountUtil;
import com.epam.andrei_sterkhov.task2.util.TransactionUtil;
import com.epam.andrei_sterkhov.task2.util.TransferEmulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionUtil.class);
    private static final int NUMBER_OF_ACCOUNTS = 10;
    private static final int NUMBER_OF_TRANSFERS = 1000;
    private static final int NUMBER_OF_TREAD_POOL = 10;
    private static final int NUMBER_OF_TREADS = 10;


    public static void main(String[] args) {
        //Задержка для visualvm
        /*try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.error("", e);
        }*/
        // Папка для хранения наших аккаунтов
        String folderName = "src/main/java/com/epam/andrei_sterkhov/task2/accounts";

        // Генератор
        Generator generator = new Generator();
        // Утилита для работы с аккаунтами
        AccountUtil accountUtil = new AccountUtil(folderName);


        // Создаем наши аккаунты
        List<Account> accounts = generator.generateListAccounts(NUMBER_OF_ACCOUNTS);

        // Записали в папку
        accountUtil.writeAccountsToFolder(accounts);

        // Получаем начальный баланс по всем счетам
        Long allBalance = accountUtil.getBalanceFromFolder();
        LOGGER.info("Общий баланс счетов: {}", allBalance);
        LOGGER.info("Аккаунты: {}", accounts);

        // Создаем executor
        ExecutorService executor = Executors.newFixedThreadPool(NUMBER_OF_TREAD_POOL);

        // Создаем transactionUtil для совершения переводов
        TransactionUtil transactionUtil = new TransactionUtil(accounts, NUMBER_OF_TRANSFERS, executor);

        // Создаем экземпляр Runnable
        TransferEmulator transferEmulator = new TransferEmulator(transactionUtil);

        // Создаем потоки
        List<Future<?>> results = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_TREADS; i++) {
            Future<?> result = executor.submit(transferEmulator);
            results.add(result);
        }
        // Ждем когда все потоки завершаться
        for (Future<?> result : results) {
            try {
                result.get();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                LOGGER.error("Interrupt of Future", e);
            } catch (ExecutionException e) {
                LOGGER.error("Execution error", e);
            }
        }

        // Получаем наши аккаунты после всех переводов
        accounts = transactionUtil.getAccounts();

        // Записываем обратно в папку
        accountUtil.writeAccountsToFolder(accounts);

        allBalance = accountUtil.getBalanceFromList(accounts);

        LOGGER.info("Общий баланс счетов: {}", allBalance);
        LOGGER.info("Аккаунты: {}", accounts);
        LOGGER.info("Совершено переводов: {}", transactionUtil.getCounter());
    }
}
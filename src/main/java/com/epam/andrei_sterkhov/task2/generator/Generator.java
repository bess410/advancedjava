package com.epam.andrei_sterkhov.task2.generator;

import com.epam.andrei_sterkhov.task2.dao.Account;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {
    private List<Account> accounts;
    private static Random random = new Random();

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public Account generateAccount() {
        Account account = new Account();
        String[] names = {"Саша", "Петя", "Вася", "Коля", "Глеб", "Витя", "Вадим", "Андрей", "Стас", "Влад", "Филипп"};
        account.setUserName(names[random.nextInt(names.length)]);
        account.setBalance(random.nextInt(50) * 1000L);
        return account;
    }

    public List<Account> generateListAccounts(int number) {
        List<Account> accountList = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            accountList.add(generateAccount());
        }
        return accountList;
    }

    public Account getRandomAccount() {
        return accounts.get(new Random().nextInt(accounts.size()));
    }

    public Long getRandomAmount() {
        return (long) ((new Random().nextInt(50)) * 1000);
    }
}

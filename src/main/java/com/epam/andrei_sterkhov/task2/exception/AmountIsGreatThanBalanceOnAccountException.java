package com.epam.andrei_sterkhov.task2.exception;

public class AmountIsGreatThanBalanceOnAccountException extends Exception {
    public AmountIsGreatThanBalanceOnAccountException(String message) {
        super(message);
    }
}

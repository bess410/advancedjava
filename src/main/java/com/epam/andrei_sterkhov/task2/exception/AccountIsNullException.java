package com.epam.andrei_sterkhov.task2.exception;

public class AccountIsNullException extends Exception {
    public AccountIsNullException(String message) {
        super(message);
    }
}


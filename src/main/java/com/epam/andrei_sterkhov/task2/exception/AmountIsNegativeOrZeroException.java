package com.epam.andrei_sterkhov.task2.exception;

public class AmountIsNegativeOrZeroException extends Exception {
    public AmountIsNegativeOrZeroException(String message) {
        super(message);
    }
}

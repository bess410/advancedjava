package com.epam.andrei_sterkhov.task2.exception;

public class TryTransferToSameAccountException extends Exception {
    public TryTransferToSameAccountException(String message) {
        super(message);
    }
}

package com.epam.andrei_sterkhov.task2.exception;

public class WrongAccountException extends Exception {
    public WrongAccountException(String message) {
        super(message);
    }
}

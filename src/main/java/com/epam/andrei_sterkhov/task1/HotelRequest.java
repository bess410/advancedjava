package com.epam.andrei_sterkhov.task1;

public class HotelRequest {
    private String hotelName;
    private int seatRoom;
    private int cost;

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public int getSeatRoom() {
        return seatRoom;
    }

    public void setSeatRoom(int seatRoom) {
        this.seatRoom = seatRoom;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "HotelRequest{" +
                "hotelName='" + hotelName + '\'' +
                ", seatRoom=" + seatRoom +
                ", cost=" + cost +
                '}';
    }
}

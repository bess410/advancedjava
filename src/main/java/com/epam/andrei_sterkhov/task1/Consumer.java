package com.epam.andrei_sterkhov.task1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Consumer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);
    private MyQueue sharedList;
    private static final int TIME = 5000;

    public Consumer(MyQueue sharedList) {
        this.sharedList = sharedList;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            HotelRequest request = sharedList.removeFirst();
            processRequest(request);
        }
    }

    private void processRequest(HotelRequest request) {
        if (request != null) {
            try {
                Thread.sleep(TIME);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                LOGGER.error("Process operation failed: ", e);
            }
            LOGGER.info("processed {} SizeList={}", request, sharedList.size());
        }
    }
}
package com.epam.andrei_sterkhov.task1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;

public class MyQueue {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyQueue.class);

    private final LinkedList<HotelRequest> queue = new LinkedList<>();
    private final int capacity;
    private volatile int messageCounter = 0;
    private final int numberOfMessages;

    public MyQueue(int capacity, int numberOfMessages) {
        this.capacity = capacity;
        this.numberOfMessages = numberOfMessages;
    }

    public int size() {
        return queue.size();
    }

    public synchronized void add(HotelRequest request) {
        // Если сообщений отправлено меньше, чем нужно
        if (messageCounter < numberOfMessages) {
            // Проверяем полная ли очередь
            // Пока очередь полная, то ждем
            while (size() == capacity) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    LOGGER.error("Thread is interupted from wait(): ", e);
                }
            }
            // Проверяем количество сообщений после сна
            if (messageCounter < numberOfMessages) {
                // добавляем элемент
                queue.add(request);
                LOGGER.info("sent {} SizeList={}", request, size());
                messageCounter++;
                // Состояние очереди изменилось => будим нити
                notifyAll();
                return;
            }
        }
        // Прерываем текущую нить
        Thread.currentThread().interrupt();
    }

    private boolean isEmpty() {
        return queue.isEmpty();
    }

    public synchronized HotelRequest removeFirst() {
        // Пока очередь пустая
        while (isEmpty()) {
            // Проверяем, если отправлены все сообщения, то прерываем нить
            // и выходим из метода
            if (messageCounter == numberOfMessages) {
                Thread.currentThread().interrupt();
                return null;
            } else {
                // Иначе ждем
                try {
                    wait();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    LOGGER.error("Thread is interupted from wait(): ", e);
                }
            }
        }

        HotelRequest request = queue.removeFirst();
        LOGGER.info("received {} SizeList={}", request, size());

        // Состояние очереди изменилось => будем нити
        notifyAll();
        return request;
    }
}
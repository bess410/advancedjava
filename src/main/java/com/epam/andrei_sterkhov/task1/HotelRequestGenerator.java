package com.epam.andrei_sterkhov.task1;

import java.util.Random;

public class HotelRequestGenerator {
    public HotelRequest generateHotelRequest() {
        HotelRequest hotelRequest = new HotelRequest();
        Random random = new Random();
        String[] arrHotelNames = {"Star Wars", "Бокстел", "Есенин", "ICON", "Москва Киевская"};
        hotelRequest.setHotelName(arrHotelNames[random.nextInt(arrHotelNames.length)]);
        hotelRequest.setCost((random.nextInt(10) + 1) * 1000);
        hotelRequest.setSeatRoom(random.nextInt(4) + 1);
        return hotelRequest;
    }
}

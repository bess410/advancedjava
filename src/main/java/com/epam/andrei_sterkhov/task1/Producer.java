package com.epam.andrei_sterkhov.task1;

public class Producer implements Runnable {
    private MyQueue sharedList;
    private static HotelRequestGenerator generator = new HotelRequestGenerator();

    public Producer(MyQueue sharedList) {
        this.sharedList = sharedList;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            HotelRequest request = generator.generateHotelRequest();
            sharedList.add(request);
        }
    }
}
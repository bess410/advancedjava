package com.epam.andrei_sterkhov.task1;

public class TestApp {
    public static void main(String[] args) {
        MyQueue sharedList = new MyQueue(5, 15);

        for (int i = 0; i < 3; i++) {
            Thread thread = new Thread(new Producer(sharedList));
            thread.setName("Producer-" + (i + 1));
            thread.start();
        }
        for (int i = 0; i < 6; i++) {
            Thread thread = new Thread(new Consumer(sharedList));
            thread.setName("Consumer-" + (i + 1));
            thread.start();
        }
    }
}
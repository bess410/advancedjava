package com.epam.andrei_sterkhov.task3;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Secured2(myNumber = 10)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Secured {
    int value();

    String strictness() default "strict";
}

package com.epam.andrei_sterkhov.task3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectionUses {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReflectionUses.class);
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        //1. Create instance of this class using hardcoded string class name
        LOGGER.info("Task1");
        String clsName = "com.epam.andrei_sterkhov.task3.Entity";
        Class clazz = Class.forName(clsName);
        Entity entity = (Entity)clazz.newInstance();
        LOGGER.info("Entity is {}", entity);

        //2.print out all methods of it
        LOGGER.info("Task2");
        Method[] methods = clazz.getDeclaredMethods();
        Arrays.stream(methods).forEach(method -> LOGGER.info("method: {}", method.getName()));

        //3.for each method show annotation presense and its parameters
        LOGGER.info("Task3");
        for (Method method : methods) {
            Annotation[] annotations = method.getDeclaredAnnotations();
            LOGGER.info("{} :",  method.getName());
            if (annotations.length == 0) {
                LOGGER.info("doesn't have annotations");
            } else {
                Arrays.stream(annotations).forEach(annotation -> LOGGER.info("{}", annotation));
            }
        }
    }
}

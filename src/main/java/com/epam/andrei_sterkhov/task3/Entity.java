package com.epam.andrei_sterkhov.task3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Entity {
    private static final Logger LOGGER = LoggerFactory.getLogger(Entity.class);

    @Secured(35)
    public void publicMethod(int x) {
        LOGGER.info("In the public method with parameter x = {}", x);
    }

    @Secured(value = 12, strictness = "notStrict")
    private void privateMethod(int x, int y) {
        LOGGER.info("In the private method with parametes: x = {} and y = {}", x, y);
    }

    protected void protectedMethod() {
        LOGGER.info("In the protected method");
    }

    void packageMethod() {
        LOGGER.info("In the private-package method");
    }
}
